# Esko Arajärvi <edu@iki.fi>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: mailgraph\n"
"Report-Msgid-Bugs-To: mailgraph@packages.debian.org\n"
"POT-Creation-Date: 2010-10-12 06:32+0200\n"
"PO-Revision-Date: 2010-01-09 21:50+0200\n"
"Last-Translator: Esko Arajärvi <edu@iki.fi>\n"
"Language-Team: Finnish <debian-l10n-finnish@lists.debian.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Finnish\n"
"X-Poedit-Country: FINLAND\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Type: boolean
#. Description
#: ../mailgraph.templates:1001
msgid "Should Mailgraph start on boot?"
msgstr "Tulisiko Mailgraph käynnistää käynnistettäessä järjestelmä?"

#. Type: boolean
#. Description
#: ../mailgraph.templates:1001
msgid ""
"Mailgraph can start on boot time as a daemon. Then it will monitor your "
"Postfix logfile for changes. This is recommended."
msgstr ""
"Mailgraph voidaan käynnistää taustaohjelmana käynnistettäessä järjestelmä. "
"Tällöin se alkaa seuraamaan Postfixin lokitiedoston muutoksia. Tätä "
"suositellaan."

#. Type: boolean
#. Description
#: ../mailgraph.templates:1001
msgid "The other method is to call mailgraph by hand with the -c parameter."
msgstr ""
"Toinen mahdollinen tapa on kutsua mailgraphia käsin parametrin -c kanssa."

#. Type: string
#. Description
#: ../mailgraph.templates:2001
msgid "Logfile used by mailgraph:"
msgstr "Ohjelman mailgraph käyttämä lokitiedosto:"

#. Type: string
#. Description
#: ../mailgraph.templates:2001
msgid ""
"Enter the logfile which should be used to create the databases for "
"mailgraph. If unsure, leave default (/var/log/mail.log)."
msgstr ""
"Anna lokitiedosto, jota tulisi käyttää mailgraphin tietokantojen luomiseen. "
"Jos olet epävarma, käytä oletusarvoa (/var/log/mail.log)."

#. Type: boolean
#. Description
#: ../mailgraph.templates:3001
msgid "Ignore mail to/from localhost?"
msgstr "Jätetäänkö paikalliset viestit huomiotta?"

#. Type: boolean
#. Description
#: ../mailgraph.templates:3001
msgid ""
"When using a content filter like amavis, incoming mail is counted more than "
"once, which will result in wrong values. If you use some content filter, you "
"should choose this option."
msgstr ""
"Käytettäessä sisältösuodattimia kuten amavis, tulevat viestit lasketaan "
"useammin kuin kerran ja tulokset vääristyvät. Valitse tämä vaihtoehto, jos "
"käytössä on jokin sisältösuodatin."
